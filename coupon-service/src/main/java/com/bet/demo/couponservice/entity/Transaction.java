package com.bet.demo.couponservice.entity;

import com.bet.demo.couponservice.boundary.TransactionStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "transaction_sequence")
public class Transaction extends BaseEntity {

    @Enumerated(EnumType.STRING)
    private TransactionStatus status;

    private BigDecimal amount;

    private String description;

    @ManyToMany(cascade = {
            CascadeType.MERGE
    })
    @JoinTable(name = "transaction_coupon",
            joinColumns = @JoinColumn(name = "transaction_id"),
            inverseJoinColumns = @JoinColumn(name = "coupon_id")
    )
    private List<Coupon> coupons = new ArrayList<>();
}
