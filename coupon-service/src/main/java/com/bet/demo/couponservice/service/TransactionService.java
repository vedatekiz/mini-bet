package com.bet.demo.couponservice.service;

import com.bet.demo.couponservice.entity.Transaction;

public interface TransactionService {

    Transaction findTransactionByIdWithCoupon(Long transactionId);

}
