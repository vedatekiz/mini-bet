package com.bet.demo.couponservice.repository;

import com.bet.demo.couponservice.entity.CouponSelection;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CouponSelectionRepository extends JpaRepository<CouponSelection, Long> {

    List<CouponSelection> findByCouponId(Long couponId);

}
