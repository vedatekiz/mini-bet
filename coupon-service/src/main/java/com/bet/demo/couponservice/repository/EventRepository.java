package com.bet.demo.couponservice.repository;

import com.bet.demo.couponservice.boundary.EventType;
import com.bet.demo.couponservice.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface EventRepository extends JpaRepository<Event, Long> {

    List<Event> findByMbs(Integer mbs);

    List<Event> findByType(EventType type);

    List<Event> findByTypeAndEventDateIsAfter(EventType type, Date eventDate);

    List<Event> findByEventDateBefore(Date date);

    List<Event> findByEventDateAfter(Date date);
}
