package com.bet.demo.couponservice.repository;

import com.bet.demo.couponservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
