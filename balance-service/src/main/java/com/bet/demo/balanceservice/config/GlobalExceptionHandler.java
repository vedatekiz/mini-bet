package com.bet.demo.balanceservice.config;

import com.bet.demo.balanceservice.boundary.ApiResponse;
import com.bet.demo.balanceservice.exception.EntityNotFoundException;
import com.bet.demo.balanceservice.exception.InsufficientBalanceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class {@code GlobalExceptionHandler} is the main exception handling class that {@code ApiResponse} object is manipulated
 * <p>
 * Please see the {@link com.bet.demo.balanceservice.boundary.ApiResponse} class for actual response object that is returned
 *
 * @author Ali Vedat Ekiz
 */

@RestControllerAdvice
public class GlobalExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        logger.error("Method Arguments Not Valid");
        ApiResponse apiResponse;
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        if (Objects.isNull(allErrors) || allErrors.isEmpty()) {
            apiResponse = new ApiResponse("Method Arguments Not Valid");
        } else {
            List<String> errorList = allErrors.stream().map(objectError -> objectError.getDefaultMessage() + " on object " + objectError.getObjectName()).collect(Collectors.toList());
            apiResponse = new ApiResponse("Method Arguments Not Valid", errorList);
        }
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ApiResponse> handleEntityNotFoundException(EntityNotFoundException e) {
        logger.error("Entity Not Found");
        ApiResponse apiResponse = new ApiResponse(e.getMessage());
        return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InsufficientBalanceException.class)
    public ResponseEntity<ApiResponse> handleInsufficientBalanceException(InsufficientBalanceException e) {
        logger.error("InsufficientBalanceException");
        ApiResponse apiResponse = new ApiResponse(e.getMessage());
        return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> handleException(Exception e) {
        logger.error("Unhandled Exception");
        logger.error(e.getMessage());
        ApiResponse apiResponse = new ApiResponse("System Error");
        return new ResponseEntity<>(apiResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
