package com.bet.demo.balanceservice.entity;

import com.bet.demo.balanceservice.boundary.OperationType;
import com.bet.demo.balanceservice.boundary.Status;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.SequenceGenerator;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "userbalance_history_sequence")
public class UserBalanceHistory extends BaseEntity {

    private Long transactionId;

    @Enumerated(EnumType.STRING)
    private OperationType operationType;

    private BigDecimal amount;

    private String description;

    @Enumerated(EnumType.STRING)
    private Status status;

}
