package com.bet.demo.couponservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "user_sequence")
public class User extends BaseEntity {

    private String username;

    private String name;

    private String surname;

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = false
    )
    private List<Coupon> coupons = new ArrayList<>();
}
