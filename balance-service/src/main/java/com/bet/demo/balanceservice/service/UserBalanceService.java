package com.bet.demo.balanceservice.service;

import com.bet.demo.balanceservice.boundary.OperationType;

import java.math.BigDecimal;

public interface UserBalanceService {

    void changeBalance(Long userId, Long transactionId, BigDecimal amount, OperationType operationType);

}
