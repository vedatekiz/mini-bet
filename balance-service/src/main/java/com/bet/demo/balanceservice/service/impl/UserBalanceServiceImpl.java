package com.bet.demo.balanceservice.service.impl;

import com.bet.demo.balanceservice.boundary.OperationType;
import com.bet.demo.balanceservice.boundary.Status;
import com.bet.demo.balanceservice.entity.UserBalance;
import com.bet.demo.balanceservice.entity.UserBalanceHistory;
import com.bet.demo.balanceservice.exception.EntityNotFoundException;
import com.bet.demo.balanceservice.exception.InsufficientBalanceException;
import com.bet.demo.balanceservice.repository.UserBalanceRepository;
import com.bet.demo.balanceservice.service.UserBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;

@Service
public class UserBalanceServiceImpl implements UserBalanceService {

    @Autowired
    UserBalanceRepository userBalanceRepository;

    @Override
    public void changeBalance(Long userId, Long transactionId, BigDecimal amount, OperationType operationType) {
//        UserBalance userBalance = userBalanceRepository.findByUserId(userId);
        UserBalance userBalance = userBalanceRepository.findByUserIdWithHistory(userId);
        if (Objects.isNull(userBalance)) {
            throw new EntityNotFoundException(String.format("User Balance is not found for user id: %s", userId));
        }

        UserBalanceHistory userBalanceHistory;
        try {
            if (OperationType.PLAY.equals(operationType)) {
                userBalance.decreaseBalance(amount);
            } else {
                userBalance.increaseBalance(amount);
            }
            userBalanceHistory = UserBalanceHistory.builder()
                    .transactionId(transactionId)
                    .amount(amount)
                    .operationType(operationType)
                    .status(Status.SUCCESS)
                    .build();
            userBalance.getHistory().add(userBalanceHistory);
            userBalanceRepository.save(userBalance);
        } catch (InsufficientBalanceException ibe) {
            userBalanceHistory = UserBalanceHistory.builder()
                    .transactionId(transactionId)
                    .amount(amount)
                    .operationType(operationType)
                    .status(Status.ERROR)
                    .description("Balance is insufficient")
                    .build();
            userBalance.getHistory().add(userBalanceHistory);
            userBalanceRepository.save(userBalance);
            throw ibe;
        }


    }
}
