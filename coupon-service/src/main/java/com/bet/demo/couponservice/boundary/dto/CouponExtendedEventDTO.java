package com.bet.demo.couponservice.boundary.dto;

import com.bet.demo.couponservice.boundary.CouponStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class CouponExtendedEventDTO {

    private Long id;

    private BigDecimal cost;

    private CouponStatus status;

    private Date playDate;

    private List<EventQueryDTO> events;

}
