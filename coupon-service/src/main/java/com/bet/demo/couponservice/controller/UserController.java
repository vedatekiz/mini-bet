package com.bet.demo.couponservice.controller;

import com.bet.demo.couponservice.boundary.ApiResponse;
import com.bet.demo.couponservice.boundary.CouponStatus;
import com.bet.demo.couponservice.boundary.dto.CouponExtendedEventDTO;
import com.bet.demo.couponservice.entity.Coupon;
import com.bet.demo.couponservice.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UserService userService;

    @GetMapping(path = "/{userId}/playedCoupons")
    public ResponseEntity<ApiResponse> getClient(@PathVariable Long userId) {
        List<Coupon> coupons = userService.listCouponsOfUser(userId, CouponStatus.PLAYED);
        List<CouponExtendedEventDTO> couponExtendedEventDTOS = coupons.stream().map(coupon -> objectMapper.convertValue(coupon, CouponExtendedEventDTO.class)).collect(Collectors.toList());
        ApiResponse apiResponse = new ApiResponse("Played Coupons of User Successfully Returned", couponExtendedEventDTOS);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

}
