package com.bet.demo.couponservice.boundary;

public enum TransactionStatus {
    INITIAL_PLAY, INITIAL_CANCEL, SUCCESS, ERROR;
}
