package com.bet.demo.couponservice.service;

import com.bet.demo.couponservice.entity.Event;

import java.util.List;

public interface EventService {

    List<Event> getAllEvents();

}
