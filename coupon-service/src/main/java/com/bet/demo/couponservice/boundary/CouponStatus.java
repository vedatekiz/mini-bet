package com.bet.demo.couponservice.boundary;

public enum CouponStatus {
    READY, PLAY_REQUEST, PLAY_REJECTED, PLAYED, CANCEL_REQUEST, CANCEL_REJECTED, CANCELED;
}
