package com.bet.demo.couponservice.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "coupon_selection_sequence")
public class CouponSelection extends BaseEntity {

    private Long couponId;

    private Long eventId;

}
