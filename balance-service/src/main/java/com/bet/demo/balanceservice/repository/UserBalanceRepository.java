package com.bet.demo.balanceservice.repository;

import com.bet.demo.balanceservice.entity.UserBalance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserBalanceRepository extends JpaRepository<UserBalance, Long> {

    UserBalance findByUserId(Long userId);

    @Query("select ub from UserBalance ub left join fetch ub.history where ub.userId =:userId")
    UserBalance findByUserIdWithHistory(@Param("userId") Long userId);
}
