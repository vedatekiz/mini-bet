package com.bet.demo.balanceservice.repository;

import com.bet.demo.balanceservice.entity.UserBalanceHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserBalanceHistoryRepository extends JpaRepository<UserBalanceHistory, Long> {
}
