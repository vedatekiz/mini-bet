package com.bet.demo.couponservice.entity;

import com.bet.demo.couponservice.boundary.EventType;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.SequenceGenerator;
import java.util.Date;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "event_sequence")
public class Event extends BaseEntity {

    private String name;

    private Integer mbs;

    @Enumerated(EnumType.STRING)
    private EventType type;

    private Date eventDate;

}
