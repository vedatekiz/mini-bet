package com.bet.demo.couponservice.boundary.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class CouponCreateDTO {

    private BigDecimal cost;

    private Date playDate;

    private List<Long> events;

}
