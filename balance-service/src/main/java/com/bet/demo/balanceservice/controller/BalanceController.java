package com.bet.demo.balanceservice.controller;

import com.bet.demo.balanceservice.boundary.ApiResponse;
import com.bet.demo.balanceservice.boundary.dto.BalanceRequestDTO;
import com.bet.demo.balanceservice.service.UserBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("balance")
public class BalanceController {

    @Autowired
    UserBalanceService userBalanceService;

    @PostMapping
    public ResponseEntity<ApiResponse> changeBalance(@RequestBody @Validated BalanceRequestDTO balanceRequestDTO) {
        userBalanceService.changeBalance(balanceRequestDTO.getUserId(), balanceRequestDTO.getTransactionId(), balanceRequestDTO.getTotalCost(), balanceRequestDTO.getOperationType());
        ApiResponse apiResponse = new ApiResponse("Balance Changed Successfully");
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
