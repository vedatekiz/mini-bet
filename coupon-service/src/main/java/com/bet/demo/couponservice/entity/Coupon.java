package com.bet.demo.couponservice.entity;

import com.bet.demo.couponservice.boundary.CouponStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "coupon_sequence")
public class Coupon extends BaseEntity {

    private static final BigDecimal COST = new BigDecimal(5);

    private BigDecimal cost = COST;

    @Enumerated(EnumType.STRING)
    private CouponStatus status;

    private Date playDate;

    private Date createDate;

    private Date updateDate;

    @Transient
    private List<Event> events = new ArrayList<>();

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public void makeReadyCoupon(){
        this.user = null;
        this.status = CouponStatus.READY;
    }
}
