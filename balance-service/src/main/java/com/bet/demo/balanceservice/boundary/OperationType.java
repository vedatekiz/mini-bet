package com.bet.demo.balanceservice.boundary;

public enum OperationType {
    PLAY, CANCEL;
}
