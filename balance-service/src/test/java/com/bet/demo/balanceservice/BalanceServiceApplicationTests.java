package com.bet.demo.balanceservice;

import com.bet.demo.balanceservice.boundary.OperationType;
import com.bet.demo.balanceservice.boundary.Status;
import com.bet.demo.balanceservice.entity.UserBalance;
import com.bet.demo.balanceservice.exception.EntityNotFoundException;
import com.bet.demo.balanceservice.exception.InsufficientBalanceException;
import com.bet.demo.balanceservice.repository.UserBalanceRepository;
import com.bet.demo.balanceservice.service.UserBalanceService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BalanceServiceApplicationTests {

    @Autowired
    UserBalanceService userBalanceService;

    @Autowired
    UserBalanceRepository userBalanceRepository;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testPlayCoupon() {
        Long userId = 1L;
        Long transactionId = 1L;
        BigDecimal amount = new BigDecimal(5);
        UserBalance userBalanceBefore = userBalanceRepository.findByUserIdWithHistory(userId);
        userBalanceService.changeBalance(userId, transactionId, amount, OperationType.PLAY);
        UserBalance userBalanceAfter = userBalanceRepository.findByUserIdWithHistory(userId);
        Boolean balanceOk = userBalanceBefore.getBalance().equals(userBalanceAfter.getBalance().add(amount));

        Boolean historyOk = userBalanceAfter.getHistory().stream().filter(history -> {
            return !userBalanceBefore.getHistory().contains(history);
        }).allMatch(h -> (h.getAmount().compareTo(amount) == 0 && h.getTransactionId().equals(transactionId) && h.getStatus().equals(Status.SUCCESS) && h.getOperationType().equals(OperationType.PLAY)));

        Assert.assertEquals(true, balanceOk && historyOk);
    }

    @Test
    public void testCancelCoupon() {
        Long userId = 1L;
        Long transactionId = 1L;
        BigDecimal amount = new BigDecimal(5);
        UserBalance userBalanceBefore = userBalanceRepository.findByUserIdWithHistory(userId);
        userBalanceService.changeBalance(userId, transactionId, amount, OperationType.CANCEL);
        UserBalance userBalanceAfter = userBalanceRepository.findByUserIdWithHistory(userId);
        Boolean balanceOk = userBalanceBefore.getBalance().equals(userBalanceAfter.getBalance().subtract(amount));

        Boolean historyOk = userBalanceAfter.getHistory().stream().filter(history -> {
            return !userBalanceBefore.getHistory().contains(history);
        }).allMatch(h -> (h.getAmount().compareTo(amount) == 0 && h.getTransactionId().equals(transactionId) && h.getStatus().equals(Status.SUCCESS) && h.getOperationType().equals(OperationType.CANCEL)));

        Assert.assertEquals(true, balanceOk && historyOk);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testChangeBalanceWhenUserBalanceNotFound() {
        userBalanceService.changeBalance(10L, 10000L, new BigDecimal(50), OperationType.PLAY);
    }

    @Test(expected = InsufficientBalanceException.class)
    public void testChangeBalanceWhenInsufficientBalance() {
        Long userId = 1L;
        userBalanceService.changeBalance(userId, 1L, new BigDecimal(5000), OperationType.PLAY);
    }

}
