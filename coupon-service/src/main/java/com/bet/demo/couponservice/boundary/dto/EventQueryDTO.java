package com.bet.demo.couponservice.boundary.dto;

import com.bet.demo.couponservice.boundary.EventType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class EventQueryDTO {

    private Long id;

    private String name;

    private Integer mbs;

    private EventType type;

    private Date eventDate;
}
