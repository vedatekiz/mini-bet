package com.bet.demo.couponservice.exception;

import lombok.Getter;

/**
 * Class {@code CouponCreateBusinessException} exception class that is thrown when coupon rules are not satisfied
 *
 * @author Ali Vedat Ekiz
 */

@Getter
public class CouponPlayBusinessException extends RuntimeException {

    private String message;

    public CouponPlayBusinessException(String message) {
        super();
        this.message = message;
    }
}
