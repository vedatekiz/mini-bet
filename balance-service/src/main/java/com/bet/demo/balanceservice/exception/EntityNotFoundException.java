package com.bet.demo.balanceservice.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Class {@code EntityNotFoundException} exception class that is thrown when the requested entity not found in the database
 *
 * @author Ali Vedat Ekiz
 */

@Getter
@Setter
public class EntityNotFoundException extends RuntimeException {

    private String message;

    public EntityNotFoundException(String message) {
        super();
        this.message = message;
    }
}
