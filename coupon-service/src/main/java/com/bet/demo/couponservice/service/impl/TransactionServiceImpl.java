package com.bet.demo.couponservice.service.impl;

import com.bet.demo.couponservice.entity.Transaction;
import com.bet.demo.couponservice.repository.TransactionRepository;
import com.bet.demo.couponservice.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Transaction findTransactionByIdWithCoupon(Long transactionId) {
        return transactionRepository.findTransactionByIdWithCoupon(transactionId);
    }
}
