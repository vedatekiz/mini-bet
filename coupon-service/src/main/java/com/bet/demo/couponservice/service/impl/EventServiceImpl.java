package com.bet.demo.couponservice.service.impl;

import com.bet.demo.couponservice.entity.Event;
import com.bet.demo.couponservice.repository.EventRepository;
import com.bet.demo.couponservice.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    EventRepository eventRepository;

    @Cacheable("events")
    @Override
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }
}
