package com.bet.demo.couponservice.repository;

import com.bet.demo.couponservice.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("select t from Transaction  t left join fetch t.coupons where t.id = :transactionId")
    Transaction findTransactionByIdWithCoupon(@Param("transactionId") Long transactionId);

}
