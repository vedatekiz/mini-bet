package com.bet.demo.couponservice.controller;

import com.bet.demo.couponservice.boundary.ApiResponse;
import com.bet.demo.couponservice.boundary.CouponStatus;
import com.bet.demo.couponservice.boundary.dto.CouponCreateDTO;
import com.bet.demo.couponservice.boundary.dto.CouponExtendedEventDTO;
import com.bet.demo.couponservice.boundary.dto.CouponPlayDTO;
import com.bet.demo.couponservice.boundary.dto.TransactionDTO;
import com.bet.demo.couponservice.entity.Transaction;
import com.bet.demo.couponservice.repository.TransactionRepository;
import com.bet.demo.couponservice.service.CouponService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("coupons")
public class CouponController {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    CouponService couponService;

    @Autowired
    TransactionRepository transactionRepository;

    @PostMapping(path = "/createReady")
    public ResponseEntity<ApiResponse> createReadyCoupon(@RequestBody @Validated CouponCreateDTO couponCreateDTO) {
        Long couponId = couponService.createReadyCoupon(couponCreateDTO);
        ApiResponse apiResponse = new ApiResponse("Ready Coupon created successfully", couponId);
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    @GetMapping(path = "/listReady")
    public ResponseEntity<ApiResponse> listReadyCoupons() {
        List<CouponExtendedEventDTO> couponExtendedEventDTOS = couponService.listReadyCoupons().stream().map(coupon -> objectMapper.convertValue(coupon, CouponExtendedEventDTO.class)).collect(Collectors.toList());
        ApiResponse apiResponse = new ApiResponse("Ready Coupon List Successfully Returned", couponExtendedEventDTOS);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping(path = "/list")
    public ResponseEntity<ApiResponse> listCoupons(@RequestParam String status) {
        try {
            CouponStatus couponStatus = CouponStatus.valueOf(status);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ApiResponse("Not a valid status"), HttpStatus.BAD_REQUEST);
        }
        List<CouponExtendedEventDTO> couponExtendedEventDTOS = couponService.listCouponsByStatus(status).stream().map(coupon -> objectMapper.convertValue(coupon, CouponExtendedEventDTO.class)).collect(Collectors.toList());
        ApiResponse apiResponse = new ApiResponse("Coupon List Successfully Returned", couponExtendedEventDTOS);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PostMapping(path = "/play")
    public ResponseEntity<ApiResponse> playCoupon(@RequestBody @Validated CouponPlayDTO couponPlayDTO) {
        Transaction transaction = couponService.playCoupon(couponPlayDTO.getUserId(), couponPlayDTO.getCouponIds());
        ApiResponse apiResponse = new ApiResponse(objectMapper.convertValue(transaction, TransactionDTO.class));
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PostMapping(path = "/cancel")
    public ResponseEntity<ApiResponse> cancelCoupon(@RequestBody @Validated CouponPlayDTO couponPlayDTO) {
        Transaction transaction = couponService.cancelCoupon(couponPlayDTO.getUserId(), couponPlayDTO.getCouponIds());
        ApiResponse apiResponse = new ApiResponse(transaction);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
