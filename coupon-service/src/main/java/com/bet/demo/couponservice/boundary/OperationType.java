package com.bet.demo.couponservice.boundary;

public enum OperationType {
    PLAY, CANCEL;
}
