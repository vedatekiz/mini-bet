package com.bet.demo.balanceservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Class {@code BaseEntity} is the base class for all the data model entities.
 *
 * Please see {@link Coupon} for subclasses
 * @author Ali Vedat Ekiz
 */

@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idgen")
    @Column(name = "id")
    private Long id;

}
