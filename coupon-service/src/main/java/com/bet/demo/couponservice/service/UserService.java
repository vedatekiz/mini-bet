package com.bet.demo.couponservice.service;

import com.bet.demo.couponservice.boundary.CouponStatus;
import com.bet.demo.couponservice.entity.Coupon;

import java.util.List;

public interface UserService {

    List<Coupon> listCouponsOfUser(Long userId, CouponStatus couponStatus);

}
