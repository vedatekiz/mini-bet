package com.bet.demo.couponservice.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Class {@code EntityNotFoundException} exception class that is thrown when the requested entity not found in the database
 *
 * @author Ali Vedat Ekiz
 */

@Getter
@Setter
public class EntityNotFoundException extends RuntimeException {

    private Class entityClass;
    private Long id;

    public EntityNotFoundException(Class clazz, Long id) {
        super();
        this.entityClass = clazz;
        this.id = id;
    }
}
