package com.bet.demo.couponservice.controller;

import com.bet.demo.couponservice.boundary.ApiResponse;
import com.bet.demo.couponservice.boundary.dto.EventQueryDTO;
import com.bet.demo.couponservice.service.EventService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("events")
public class EventController {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EventService eventService;

    @GetMapping
    public ResponseEntity<ApiResponse> getAllEvents() {
        List<EventQueryDTO> eventQueryDTOList = eventService.getAllEvents().stream().map(event -> objectMapper.convertValue(event, EventQueryDTO.class)).collect(Collectors.toList());
        return new ResponseEntity<>(new ApiResponse("Event List Successfully Returned:" + eventQueryDTOList.size(), eventQueryDTOList), HttpStatus.OK);
    }

}
