package com.bet.demo.balanceservice.exception;

import lombok.Getter;

/**
 * Class {@code InsufficientBalanceException} exception class that is thrown when balance is insufficient
 *
 * @author Ali Vedat Ekiz
 */

@Getter
public class InsufficientBalanceException extends RuntimeException {

    private String message;

    public InsufficientBalanceException(String message) {
        super();
        this.message = message;
    }
}
