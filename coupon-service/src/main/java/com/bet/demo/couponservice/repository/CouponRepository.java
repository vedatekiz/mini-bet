package com.bet.demo.couponservice.repository;

import com.bet.demo.couponservice.boundary.CouponStatus;
import com.bet.demo.couponservice.entity.Coupon;
import com.bet.demo.couponservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CouponRepository extends JpaRepository<Coupon, Long> {

    @Query(value = "SELECT c.id coupon_id, c.cost cost, e.id event_id, e.event_date, e.mbs, e.name, e.type FROM COUPON c inner join coupon_selection cs on c.id=cs.coupon_id inner join event e on e.id=cs.event_id where c.status = 'READY'", nativeQuery = true)
    List<Object[]> listReadyCoupons();

    @Query(value = "SELECT c.id coupon_id, c.cost cost, e.id event_id, e.event_date, e.mbs, e.name, e.type FROM COUPON c inner join coupon_selection cs on c.id=cs.coupon_id inner join event e on e.id=cs.event_id where c.status = :couponStatus", nativeQuery = true)
    List<Object[]> listReadyCouponsByStatus(@Param("couponStatus") String couponStatus);

    @Transactional
    @Modifying
    @Query("update Coupon c set c.status=:couponStatus where c.id in (:couponIdList)")
    void updateStatusOfCoupons(@Param("couponIdList") List<Long> couponIdList, @Param("couponStatus") CouponStatus couponStatus);

    List<Coupon> findByUserAndStatus(User user, CouponStatus couponStatus);

}
