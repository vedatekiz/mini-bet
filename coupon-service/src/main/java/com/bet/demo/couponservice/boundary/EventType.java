package com.bet.demo.couponservice.boundary;

public enum EventType {
    FOOTBALL, BASKETBALL, TENNIS;
}
