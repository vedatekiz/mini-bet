package com.bet.demo.couponservice.service.impl;

import com.bet.demo.couponservice.boundary.CouponStatus;
import com.bet.demo.couponservice.entity.Coupon;
import com.bet.demo.couponservice.entity.User;
import com.bet.demo.couponservice.exception.EntityNotFoundException;
import com.bet.demo.couponservice.repository.CouponRepository;
import com.bet.demo.couponservice.repository.UserRepository;
import com.bet.demo.couponservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CouponRepository couponRepository;

    @Override
    public List<Coupon> listCouponsOfUser(Long userId, CouponStatus couponStatus) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(User.class, userId));
        List<Coupon> couponList = couponRepository.findByUserAndStatus(user, couponStatus);
        couponList.forEach(coupon -> {
            coupon.setUser(null);
            coupon.setEvents(null);
        });
        return couponList;
    }
}
