package com.bet.demo.couponservice;

import com.bet.demo.couponservice.boundary.ApiResponse;
import com.bet.demo.couponservice.boundary.CouponStatus;
import com.bet.demo.couponservice.boundary.EventType;
import com.bet.demo.couponservice.boundary.TransactionStatus;
import com.bet.demo.couponservice.boundary.dto.CouponCreateDTO;
import com.bet.demo.couponservice.entity.Coupon;
import com.bet.demo.couponservice.entity.Event;
import com.bet.demo.couponservice.entity.Transaction;
import com.bet.demo.couponservice.exception.CouponCreateBusinessException;
import com.bet.demo.couponservice.exception.EntityNotFoundException;
import com.bet.demo.couponservice.repository.CouponRepository;
import com.bet.demo.couponservice.repository.EventRepository;
import com.bet.demo.couponservice.service.CouponService;
import com.bet.demo.couponservice.service.TransactionService;
import com.bet.demo.couponservice.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CouponServiceApplicationTests {

    @Autowired
    CouponService couponService;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    CouponRepository couponRepository;

    @Autowired
    UserService userService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @Autowired
    ObjectMapper objectMapper;

    @Value("${balance-service.playCoupon.url}")
    String balanceServiceUrl;

    @Before
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void contextLoads() {
    }

    @Test(expected = CouponCreateBusinessException.class)
    public void testMakeReadyCouponWhenMbsNotSatisfied() throws ParseException {
        CouponCreateDTO couponCreateDTO = new CouponCreateDTO();
        couponCreateDTO.setCost(new BigDecimal(5));
        couponCreateDTO.setPlayDate(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse("31.12.2020 20:00:00"));
        Event byMbs = eventRepository.findByMbs(3).get(0);
        couponCreateDTO.setEvents(new ArrayList<>(Arrays.asList(byMbs.getId())));

        couponService.createReadyCoupon(couponCreateDTO);
    }

    @Test(expected = CouponCreateBusinessException.class)
    public void testMakeReadyCouponWhenEventDateNotSatisfied() throws ParseException {
        CouponCreateDTO couponCreateDTO = new CouponCreateDTO();
        couponCreateDTO.setCost(new BigDecimal(5));
        couponCreateDTO.setPlayDate(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse("31.12.2020 20:00:00"));
        Event byMbs = eventRepository.findByEventDateBefore(new Date()).get(0);
        couponCreateDTO.setEvents(new ArrayList<>(Arrays.asList(byMbs.getId())));

        couponService.createReadyCoupon(couponCreateDTO);
    }

    @Test
    public void testMakeReadyCouponWhenEventDateSatisfied() throws ParseException {
        CouponCreateDTO couponCreateDTO = new CouponCreateDTO();
        couponCreateDTO.setCost(new BigDecimal(5));
        couponCreateDTO.setPlayDate(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse("31.12.2020 20:00:00"));
        Event byMbs = eventRepository.findByEventDateAfter(new Date()).get(0);
        couponCreateDTO.setEvents(new ArrayList<>(Arrays.asList(byMbs.getId())));

        couponService.createReadyCoupon(couponCreateDTO);
    }

    @Test(expected = CouponCreateBusinessException.class)
    public void testMakeReadyCouponWhenFootballTennisNotSatisfied() throws ParseException {
        CouponCreateDTO couponCreateDTO = new CouponCreateDTO();
        couponCreateDTO.setCost(new BigDecimal(5));
        couponCreateDTO.setPlayDate(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse("31.12.2020 20:00:00"));
        Event football = eventRepository.findByTypeAndEventDateIsAfter(EventType.FOOTBALL, new Date()).get(0);
        Event tennis = eventRepository.findByTypeAndEventDateIsAfter(EventType.TENNIS, new Date()).get(0);
        couponCreateDTO.setEvents(new ArrayList<>(Arrays.asList(football.getId(), tennis.getId())));

        couponService.createReadyCoupon(couponCreateDTO);
    }


    @Test(expected = EntityNotFoundException.class)
    public void testMakeReadyCouponWhenEventNotFound() throws ParseException {
        CouponCreateDTO couponCreateDTO = new CouponCreateDTO();
        couponCreateDTO.setCost(new BigDecimal(5));
        couponCreateDTO.setPlayDate(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse("31.12.2020 20:00:00"));
        couponCreateDTO.setEvents(new ArrayList<>(Arrays.asList(999L)));
        couponService.createReadyCoupon(couponCreateDTO);
    }

    @Test
    public void testMakeReadyCouponWhenRulesSatisfied() throws ParseException {
        Long couponId = this.createReadyCoupon();
        Assert.assertEquals(CouponStatus.READY, couponRepository.findById(couponId).get().getStatus());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testPlayCouponWhenCouponNotFound() {
        couponService.playCoupon(1L, Arrays.asList(999L));
    }

    @Test
    public void testPlayCoupon() throws ParseException, URISyntaxException, JsonProcessingException {
        Long readyCouponId = this.createReadyCoupon();
        Long readyCouponId2 = this.createReadyCoupon();
        ApiResponse apiResponse = new ApiResponse("Balance Changed Successfully");
        mockServer.expect(ExpectedCount.once(),
                requestTo(new URI(balanceServiceUrl)))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(objectMapper.writeValueAsString(apiResponse)));

        Transaction transaction = couponService.playCoupon(1L, Arrays.asList(readyCouponId, readyCouponId2));
        mockServer.verify();
        Boolean couponStatusOk = transaction.getCoupons().stream().allMatch(t -> t.getStatus().equals(CouponStatus.PLAYED));
        Assert.assertEquals(true, couponStatusOk && transaction.getStatus().equals(TransactionStatus.SUCCESS));
    }

    public Transaction findTransactionByIdWithCoupon(Long transactionId) {
        return transactionService.findTransactionByIdWithCoupon(transactionId);
    }

    @Test
    public void testCancelCoupon() throws ParseException, URISyntaxException, JsonProcessingException {
        ApiResponse apiResponse = new ApiResponse("Balance Changed Successfully");
        mockServer.expect(ExpectedCount.twice(),
                requestTo(new URI(balanceServiceUrl)))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(objectMapper.writeValueAsString(apiResponse)));

        Long userId = 1L;
        Long readyCouponId = this.createReadyCoupon();
        Transaction playTransaction = couponService.playCoupon(userId, Arrays.asList(readyCouponId));
        Transaction cancelTransaction = couponService.cancelCoupon(userId, playTransaction.getCoupons().stream().map(Coupon::getId).collect(Collectors.toList()));
        mockServer.verify();
        Boolean couponStatusOk = cancelTransaction.getCoupons().stream().allMatch(t -> t.getStatus().equals(CouponStatus.CANCELED));
        Assert.assertEquals(true, couponStatusOk);
    }

    @Test
    public void testReadyCoupons() throws ParseException {
        this.createReadyCoupon();
        this.createReadyCoupon();
        this.createReadyCoupon();
        this.createReadyCoupon();
        Assert.assertEquals(true, couponService.listReadyCoupons().stream().allMatch(t -> t.getStatus().equals(CouponStatus.READY)));
    }

    public Long createReadyCoupon() throws ParseException {
        CouponCreateDTO couponCreateDTO = new CouponCreateDTO();
        couponCreateDTO.setCost(new BigDecimal(5));
        couponCreateDTO.setPlayDate(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").parse("31.12.2020 20:00:00"));
        Event byMbs3 = eventRepository.findByMbs(3).get(0);
        Event byMbs2 = eventRepository.findByMbs(2).get(0);
        Event byMbs1 = eventRepository.findByMbs(1).get(0);
        couponCreateDTO.setEvents(new ArrayList<>(Arrays.asList(byMbs3.getId(), byMbs2.getId(), byMbs1.getId())));

        return couponService.createReadyCoupon(couponCreateDTO);
    }

}
