package com.bet.demo.balanceservice.entity;

import com.bet.demo.balanceservice.exception.InsufficientBalanceException;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@SequenceGenerator(initialValue = 1, name = "idgen", sequenceName = "userbalance_sequence")
public class UserBalance extends BaseEntity {

    @Transient
    private static final Object lock = new Object();

    private Long userId;

    private BigDecimal balance;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "user_balance_id")
    private List<UserBalanceHistory> history = new ArrayList<>();

    public void decreaseBalance(BigDecimal amount){
        synchronized (lock){
            if(amount.compareTo(balance) > 0){
                throw new InsufficientBalanceException("Balance is insufficient");
            }
            this.balance = this.balance.subtract(amount);
        }
    }

    public void increaseBalance(BigDecimal amount){
        synchronized (lock){
            this.balance = this.balance.add(amount);
        }
    }
}