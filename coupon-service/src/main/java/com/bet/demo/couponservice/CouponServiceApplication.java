package com.bet.demo.couponservice;

import com.bet.demo.couponservice.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CouponServiceApplication {

    @Autowired
    EventService eventService;

    public static void main(String[] args) {
        SpringApplication.run(CouponServiceApplication.class, args);
    }

}
