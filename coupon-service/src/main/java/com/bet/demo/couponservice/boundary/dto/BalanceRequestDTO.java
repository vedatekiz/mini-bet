package com.bet.demo.couponservice.boundary.dto;

import com.bet.demo.couponservice.boundary.OperationType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class BalanceRequestDTO {

    private Long userId;

    private Long transactionId;

    private BigDecimal totalCost;

    private OperationType operationType;

}
