package com.bet.demo.couponservice.boundary.dto;

import com.bet.demo.couponservice.boundary.TransactionStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class TransactionDTO {

    private Long id;

    private TransactionStatus status;

    private String description;

    List<CouponExtendedEventDTO> coupons;
}
