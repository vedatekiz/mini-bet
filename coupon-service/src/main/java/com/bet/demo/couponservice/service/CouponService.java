package com.bet.demo.couponservice.service;

import com.bet.demo.couponservice.boundary.dto.CouponCreateDTO;
import com.bet.demo.couponservice.entity.Coupon;
import com.bet.demo.couponservice.entity.Transaction;

import java.util.List;

public interface CouponService {

    Long createReadyCoupon(CouponCreateDTO couponCreateDTO);

    List<Coupon> listReadyCoupons();

    Transaction playCoupon(Long userId, List<Long> couponIdList);

    Transaction cancelCoupon(Long userId, List<Long> couponIdList);

    List<Coupon> listCouponsByStatus(String status);
}
