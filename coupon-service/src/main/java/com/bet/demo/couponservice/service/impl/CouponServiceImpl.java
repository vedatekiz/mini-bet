package com.bet.demo.couponservice.service.impl;

import com.bet.demo.couponservice.boundary.*;
import com.bet.demo.couponservice.boundary.dto.BalanceRequestDTO;
import com.bet.demo.couponservice.boundary.dto.CouponCreateDTO;
import com.bet.demo.couponservice.entity.*;
import com.bet.demo.couponservice.exception.CouponCreateBusinessException;
import com.bet.demo.couponservice.exception.CouponPlayBusinessException;
import com.bet.demo.couponservice.exception.EntityNotFoundException;
import com.bet.demo.couponservice.repository.*;
import com.bet.demo.couponservice.service.CouponService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
public class CouponServiceImpl implements CouponService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    CouponRepository couponRepository;

    @Autowired
    CouponSelectionRepository couponSelectionRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Value("${balance-service.playCoupon.url}")
    String balanceServiceUrl;

    @Override
    @Transactional
    @CacheEvict(value = "readyCoupons", allEntries = true)
    public Long createReadyCoupon(CouponCreateDTO couponCreateDTO) {
        List<Event> eventList = couponCreateDTO.getEvents().stream().map(eventId -> eventRepository.findById(eventId).orElseThrow(() -> new EntityNotFoundException(Event.class, eventId))).collect(Collectors.toList());
        checkCouponCreateRules(eventList);

        Coupon coupon = new Coupon();
        coupon.makeReadyCoupon();
        coupon.setCreateDate(new Date());
        coupon.setUpdateDate(new Date());
        coupon.setPlayDate(new Date());
        Long couponId = couponRepository.save(coupon).getId();

        eventList.stream().forEach(event -> {
            CouponSelection couponSelection = CouponSelection.builder().couponId(couponId).eventId(event.getId()).build();
            couponSelectionRepository.save(couponSelection);
        });

        return couponId;
    }

    private void checkCouponCreateRules(List<Event> events) {
        Integer maxMbs = 0;
        boolean footballExists = false, tennisExists = false;
        for (Event event : events) {
            if (event.getMbs() > maxMbs) {
                maxMbs = event.getMbs();
            }
            if (event.getEventDate().before(new Date())) {
                throw new CouponCreateBusinessException("Event Date should be future date");
            }
            if (EventType.FOOTBALL.equals(event.getType())) {
                footballExists = true;
            } else if (EventType.TENNIS.equals(event.getType())) {
                tennisExists = true;
            }
        }

        if (events.size() < maxMbs) {
            throw new CouponCreateBusinessException("Mbs rule is not satisfied");
        }
        if (footballExists && tennisExists) {
            throw new CouponCreateBusinessException("Football and Tennis event cannot exist in a coupon");
        }
    }

    @Override
    @Cacheable("readyCoupons")
    public List<Coupon> listReadyCoupons() {
        List<Object[]> readyCouponObject = couponRepository.listReadyCoupons();
        return mapToCouponList(readyCouponObject);
    }

    @Override
    public List<Coupon> listCouponsByStatus(String status) {
        return mapToCouponList(couponRepository.listReadyCouponsByStatus(status));
    }

    private List<Coupon> mapToCouponList(List<Object[]> objectList) {
        //Manually map resultset to Coupon Object
        List readyCouponList = new ArrayList();
        Map<Long, List<Object[]>> couponMap = objectList.stream().collect(groupingBy(obj -> ((BigInteger) obj[0]).longValue()));
        couponMap.entrySet().forEach(entrySet -> {
            Coupon coupon = new Coupon();
            coupon.setId(entrySet.getKey());
            List<Object[]> couponEventList = entrySet.getValue();
            couponEventList.forEach(couponEvent -> {
                coupon.setCost(couponEvent[1] == null ? new BigDecimal(0) : (BigDecimal) couponEvent[1]);
                coupon.setStatus(CouponStatus.READY);
                coupon.getEvents().add(Event.builder()
                        .name(couponEvent[5] == null ? "" : (String) couponEvent[5])
                        .mbs(couponEvent[4] == null ? 0 : (Integer) couponEvent[4])
                        .eventDate(couponEvent[3] == null ? null : (Date) couponEvent[3])
                        .type(couponEvent[6] == null ? null : (EventType.valueOf((String) couponEvent[6])))
                        .build());
            });
            readyCouponList.add(coupon);
        });

        return readyCouponList;
    }

    @Override
    public Transaction playCoupon(Long userId, List<Long> couponIdList) {
        //Check user existence
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(User.class, userId));
        final BigDecimal[] totalCost = {new BigDecimal(0)};
        List<Coupon> readyCouponList = new ArrayList<>();
        couponIdList.forEach(couponId -> {
            //Check coupon existence and status
            Optional<Coupon> couponOptional = couponRepository.findById(couponId);
            if (!CouponStatus.READY.equals(couponOptional.orElseThrow(() -> new EntityNotFoundException(Coupon.class, couponId)).getStatus())) {
                throw new CouponPlayBusinessException(String.format("Coupon with id %s is not a ready coupon", couponId));
            }
            Coupon coupon = couponOptional.get();
            totalCost[0] = totalCost[0].add(coupon.getCost());
            readyCouponList.add(coupon);
        });

        Transaction transaction = Transaction.builder().amount(totalCost[0]).status(TransactionStatus.INITIAL_PLAY).coupons(new ArrayList<>()).build();
        readyCouponList.forEach(coupon -> {
            //Clone new coupon and persist coupon and coupon_selection for specific user
            Coupon clonedCoupon = new Coupon();
            BeanUtils.copyProperties(coupon, clonedCoupon);
            clonedCoupon.setId(null);
            clonedCoupon.setStatus(CouponStatus.PLAY_REQUEST);
            clonedCoupon.setPlayDate(new Date());
            clonedCoupon.setCreateDate(new Date());
            clonedCoupon.setUpdateDate(new Date());
            clonedCoupon.setUser(user);
            Coupon savedCoupon = couponRepository.save(clonedCoupon);

            transaction.getCoupons().add(savedCoupon);

            List<CouponSelection> couponSelections = couponSelectionRepository.findByCouponId(coupon.getId());

            couponSelections.stream().forEach(cs -> {
                CouponSelection couponSelection = CouponSelection.builder().couponId(savedCoupon.getId()).eventId(cs.getEventId()).build();
                couponSelectionRepository.save(couponSelection);
            });
        });

        Transaction savedTransaction = transactionRepository.save(transaction);

        ResponseEntity<ApiResponse> response = null;

        try {
            BalanceRequestDTO balanceRequestDTO = BalanceRequestDTO.builder().operationType(OperationType.PLAY).transactionId(savedTransaction.getId()).totalCost(totalCost[0]).userId(userId).build();
            HttpEntity<BalanceRequestDTO> httpEntity = new HttpEntity<>(balanceRequestDTO);
            response = restTemplate.exchange(balanceServiceUrl, HttpMethod.POST, httpEntity, ApiResponse.class);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                savedTransaction.setStatus(TransactionStatus.SUCCESS);
                savedTransaction.getCoupons().forEach(coupon -> {
                    coupon.setStatus(CouponStatus.PLAYED);
                });
            } else {
                savedTransaction.setStatus(TransactionStatus.ERROR);
                savedTransaction.setDescription(response.getBody().getResponseDescription());
                savedTransaction.getCoupons().forEach(coupon -> {
                    coupon.setStatus(CouponStatus.PLAY_REJECTED);
                });
            }
        } catch (RestClientException e) {
            savedTransaction.setStatus(TransactionStatus.ERROR);
            savedTransaction.setDescription(e.getLocalizedMessage());
            savedTransaction.getCoupons().forEach(coupon -> {
                coupon.setStatus(CouponStatus.PLAY_REJECTED);
            });
        }

        return transactionRepository.save(savedTransaction);
    }

    @Override
    public Transaction cancelCoupon(Long userId, List<Long> couponIdList) {
        //Check user existence
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(User.class, userId));
        final BigDecimal[] totalCost = {new BigDecimal(0)};

        Transaction transaction = Transaction.builder().status(TransactionStatus.INITIAL_CANCEL).coupons(new ArrayList<>()).build();

        couponIdList.forEach(couponId -> {
            //Check coupon existence
            Optional<Coupon> couponOptional = couponRepository.findById(couponId);
            if (!couponOptional.isPresent()) {
                throw new EntityNotFoundException(Coupon.class, couponId);
            } else {
                Coupon coupon = couponOptional.get();
                //Check if the status of the copupons is PLAYED
                if (!CouponStatus.PLAYED.equals(coupon.getStatus())) {
                    throw new CouponPlayBusinessException(String.format("Coupons which are not played cannot be cancelled. Coupon with id %s cannot be cancelled", couponId));
                }
                //Check 5 minutes to cancel control
                LocalDateTime couponPlayDate = LocalDateTime.ofInstant(coupon.getPlayDate().toInstant(), ZoneId.systemDefault());
                if (couponPlayDate.isBefore((LocalDateTime.now().minus(5, ChronoUnit.MINUTES)))) {
                    throw new CouponPlayBusinessException(String.format("Only coupons which are played in 5 minutes can be cancelled. Coupon with id %s cannot be cancelled", couponId));
                }

                totalCost[0] = totalCost[0].add(coupon.getCost());
                transaction.getCoupons().add(coupon);
            }
        });

        transaction.setAmount(totalCost[0]);
        Transaction savedTransaction = transactionRepository.save(transaction);
        couponRepository.updateStatusOfCoupons(couponIdList, CouponStatus.CANCEL_REQUEST);

        ResponseEntity<ApiResponse> response = null;

        try {
            BalanceRequestDTO balanceRequestDTO = BalanceRequestDTO.builder().operationType(OperationType.CANCEL).transactionId(savedTransaction.getId()).totalCost(totalCost[0]).userId(userId).build();
            HttpEntity<BalanceRequestDTO> httpEntity = new HttpEntity<>(balanceRequestDTO);
            response = restTemplate.exchange(balanceServiceUrl, HttpMethod.POST, httpEntity, ApiResponse.class);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                savedTransaction.setStatus(TransactionStatus.SUCCESS);
                savedTransaction.getCoupons().forEach(coupon -> {
                    coupon.setStatus(CouponStatus.CANCELED);
                });
            } else {
                savedTransaction.setStatus(TransactionStatus.ERROR);
                savedTransaction.setDescription(response.getBody().getResponseDescription());
                savedTransaction.getCoupons().forEach(coupon -> {
                    coupon.setStatus(CouponStatus.CANCEL_REJECTED);
                });
            }
        } catch (RestClientException e) {
            savedTransaction.setStatus(TransactionStatus.ERROR);
            savedTransaction.setDescription(e.getLocalizedMessage());
            savedTransaction.getCoupons().forEach(coupon -> {
                coupon.setStatus(CouponStatus.CANCEL_REJECTED);
            });
        }

        return transactionRepository.save(savedTransaction);
    }

}
